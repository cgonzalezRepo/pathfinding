<?php

	require "../Grid.php";
	require "../Node.php";


	if (isset($_POST["wall_node_x"]) && isset($_POST["wall_node_y"])) {
		$grid->getNode($_POST["wall_node_x"],$_POST["wall_node_y"])->isWall = true;
	}

	if (isset($_POST["remove_wall_node_x"]) && isset($_POST["remove_wall_node_y"])) {
		$grid->getNode($_POST["remove_wall_node_x"],$_POST["remove_wall_node_x"])->isWall = false;
	}


	if (isset($_POST["find_path_data"])) {

		$finalResult = ['finalPath' => [], 'nodesExplored' => []];
		$data = json_decode(json_encode($_POST["find_path_data"]));

		$grid = new Grid($data->grid->width,$data->grid->height);

		$startPoint = new Node($data->start_point_node->x,$data->start_point_node->y);
		$grid->setStartPoint($startPoint);

		$endPoint = new Node($data->end_point_node->x,$data->end_point_node->y);
		$grid->setEndPoint($endPoint);

		if (isset($data->walls)) {
			foreach ($data->walls as $wall) {
				$grid->getNode($wall->x, $wall->y)->isWall = true;
			}
		}

		$nextNode = $startPoint;

		while (count($grid->openList) > 0  && $nextNode !== $endPoint) {
			//Buscamos sus vecinos
			$neighbors = $grid->getNeighbors($nextNode);

			//Agregamos los nuevos vecinos a la OpenList
			foreach ($neighbors as $neighborsNode) {
				$neighborsNode->parentNode = $nextNode;
				$neighborsNode->calculateH($endPoint);
				$neighborsNode->g = $nextNode->g + 10;
				$neighborsNode->calculateF();
				array_push($grid->openList, $neighborsNode);
				
				array_push($finalResult['nodesExplored'], $neighborsNode);
			}

			//Buscamos el valor de F más bajo en la OpenList
			$nextNode = $grid->getLowestFNode();
		}

		//Sacamos el path final
		$finalResult['finalPath'] = $grid->getPath($nextNode);
		echo json_encode($finalResult);
	}

?>