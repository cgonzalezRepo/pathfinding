$(document).ready(function() {
	generateGrid();

	setStartPoint($('#node-00'));
	setEndPoint($('#node-50'));

	$('#btnStartPoint').addClass('button-active');

	$('body').on('click', '.node', function (){
		if ($('#buttonInAction').val() == 'btnStartPoint') {
			setStartPoint($(this));
		}else if ($('#buttonInAction').val() == 'btnEndPoint') {
			setEndPoint($(this));
		} else if ($('#buttonInAction').val() == 'btnSetWall') {
			setWall($(this));
		} else if ($('#buttonInAction').val() == 'btnRemoveWall') {
			removeWall($(this));
		}
	});


	$('button').on("click", function() {
		$('button').removeClass('button-active');
		$('#buttonInAction').val($(this).attr('id'));
		$(this).addClass('button-active');
	});

	$('#btnFindPath').on("click", function() {
		findPath();
	});
});


function setStartPoint(node)
{
	var startPointNode = $("div[data-start=true]");
	startPointNode.removeAttr('data-start').removeClass('startPoint');

	node.attr('data-start', true);
	node.addClass('startPoint');
}

function setEndPoint(node)
{
	var endPointNode = $("div[data-end=true]");
	endPointNode.removeAttr('data-end').removeClass('endPoint');

	node.attr('data-end', true);
	node.addClass('endPoint');
}

function setWall(node)
{
	node.attr('data-wall', true)
	node.addClass('wallNode');
}

function removeWall(node)
{
	node.removeAttr('data-wall')
	node.removeClass('wallNode');
}

function generateGrid()
{
	var grid = $('#grid');

	for (var x = 0; x < 10; x++) {
		for (var y = 0; y < 10; y++) {
			var node = $('<div/>').attr("id", "node-"+x+y).attr("data-x", x).attr("data-y", y).addClass("node");
			var span = $('<span/>').addClass("attribute-g").attr("data-g", 0);
			node.append(span);
			var span = $('<span/>').addClass("attribute-h").attr("data-h", 0);
			node.append(span);
			var span = $('<span/>').addClass("attribute-f").attr("data-f", 0);
			node.append(span);
			grid.append(node);
		}
	}
}

function findPath()
{
	var startPointNode = $("div[data-start=true]");
	var endPointNode = $("div[data-end=true]");

	var walls = [];
	$("div[data-wall=true]").each(function() {
		var wallNode = {"x": $(this).attr('data-x'),"y": $(this).attr('data-y')};
		walls.push(wallNode);
	});


	$.ajax({
        type:'POST',
        url: 'ajax/generalCalls.php',
        dataType: 'json',
        data: {
            find_path_data: {
            	"grid" : {
            		"width" : 10,
            		"height" : 10
            	},
            	"start_point_node" : {
            		"x" : startPointNode.attr('data-x'), 
            		"y" : startPointNode.attr('data-y')
            	},
            	"end_point_node" : {
            		"x" : endPointNode.attr('data-x'), 
            		"y" : endPointNode.attr('data-y')
            	},
            	"walls" : walls
			}
        },
        success: function(data){
        	$(".node").removeClass('finalPathNode');
        	$("span").text('');
        	for (var i = 0; i < data['finalPath'].length; i++) {
        		$("#node-"+data['finalPath'][i]["x"]+data['finalPath'][i]["y"]).addClass('finalPathNode');
        	}

        	for (var j = 0; j < data['nodesExplored'].length; j++) {
        		var node = $("#node-"+data['nodesExplored'][j]["x"]+data['nodesExplored'][j]["y"]);

        		node.find('span.attribute-g').text("G: "+data['nodesExplored'][j]["g"]);
        		node.find('span.attribute-h').text("H: "+data['nodesExplored'][j]["h"]);
        		node.find('span.attribute-f').text("F: "+data['nodesExplored'][j]["f"]);
        	}
        },
        error: function(xhr, status, error) {
		  console.log(error);
		}
    });
}