<?php

	class Grid
	{
		public $xWidth;
		public $yWidth;
		public $arrayNode = [];
		public $openList = [];
		public $closedList = [];

		function __construct($a_xWidth, $a_yWidth)
		{
			$this->xWidth = $a_xWidth;
			$this->yWidth = $a_yWidth;

			for ($x=0; $x < $this->xWidth; $x++) { 
				for ($y=0; $y < $this->yWidth; $y++) { 
					$this->arrayNode[$x][$y] = new Node($x, $y);
				}
			}

		}

		public function setStartPoint($node)
		{
			$node->startNode = true;
			$this->arrayNode[$node->x][$node->y] = $node;

			//Agregamos el startPoint a la OpenList
			array_push($this->openList, $node); 
		}

		public function setEndPoint($node)
		{
			$node->endNode = true;
			$this->arrayNode[$node->x][$node->y] = $node;
		}

		public function getNode($x, $y)
		{
			if (isset($this->arrayNode[$x][$y])) {
				return $this->arrayNode[$x][$y];
			}else{
				return null;
			}
		}

		public function getNeighbors($node)
		{
			$x = $node->x;
			$y = $node->y;

			$result = [];

			$listNodes = [
				//Izquierda
				[$x-1, $y],
				//Derecha
				[$x+1, $y],
				//Arriba
				[$x, $y-1],
				//Abajo
				[$x, $y+1]
			];

			foreach ($listNodes as $lNode) {
				$neighbor = $this->getNode($lNode[0], $lNode[1]);
				$neighborInClosedList = false;
				$neighborAlredyInOpenList = false;

				if ($neighbor != null) {

					//Si esta en la closed list no los agregamos
					foreach ($this->closedList as $key => $closedListNode) {
						if ($neighbor == $closedListNode) {
							$neighborInClosedList = true;
							break;
						}
					}

					//Si esta en la open list miraremos si nos mejora para actualizarlo
					foreach ($this->openList as $key => $openListNode) {
						if ($neighbor == $openListNode) {
							$neighborAlredyInOpenList = true;
							if (($neighbor->g+10) < $openListNode->g) {
								$openListNode->g = $neighbor->g+10;
								$openListNode->parent = $neighbor->parent;
								$openListNode->calculateF();
							}

						}
					}

					if (!$neighborInClosedList && !$neighbor->isWall && !$neighborAlredyInOpenList) {
						array_push($result, $this->getNode($lNode[0], $lNode[1]));
					}
				}
			}

			//Borramos el node de la OpenList
			$this->openList = $this->removeNode($node, $this->openList);

			//Lo agregamos a la closedList ya que ya hemos buscado sus vecinos
			array_push($this->closedList, $node);

			return $result;
		}

		public function removeNode($node, $list)
		{
			foreach ($list as $key => $nodeInList) {
				if ($node->x == $nodeInList->x && $node->y == $nodeInList->y) {
					unset($list[$key]);
					return array_values($list);
				}
			}
			return $list;
		}

		public function getLowestFNode()
		{
			$lowestFNode = $this->openList[0];

			foreach ($this->openList as $node) {
				if ($node->f < $lowestFNode->f) {
					$lowestFNode = $node;
				}
			}

			return $lowestFNode;
		}

		public function getPath($node)
		{
			$endNodes = [];

			while ($node->parentNode) {
				array_push($endNodes, $node);
				$node = $node->parentNode;
			}

			return $endNodes;
		}
	}
?>