<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="app.css">
	<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
	<script src="app.js"></script>
</head>
	<body>
		<div class="main-container">
	  		<div id="grid" class="grid"></div>
	  		
	  		<div class="control-panel">
	  			<div>CONTROL PANEL</div>
	  			<button id="btnStartPoint" class="button">Set start point</button>
	  			<button id="btnEndPoint" class="button">Set end point</button>
	  			<button id="btnSetWall" class="button">Set wall</button>
	  			<button id="btnRemoveWall" class="button">Remove wall</button>
	  			<button id="btnFindPath" class="button">Find path</button>
	  		</div>
		</div>

		<input type="hidden" id="buttonInAction" value="btnStartPoint">
	</body>
</html>