<?php 

	class Node
	{
		public $x;
		public $y;
		public $startNode = false;
		public $isWall = false;
		public $explored = false;
		public $g = 0;
		public $h = 0;
		public $f = 0;
		public $parentNode;

		function __construct($a_xCoord, $a_yCoord)
		{
			$this->x = $a_xCoord;
			$this->y = $a_yCoord;
		}

		public function calculateG()
		{
			$this->g = 0;
		}

		public function calculateH($finalNode)
		{
			$this->h = (abs($this->x - $finalNode->x) + abs($this->y - $finalNode->y)) * 10;
		}

		public function calculateF()
		{
			$this->f = $this->g + $this->h;
		}
	}
?>